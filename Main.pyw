import tkinter as tk
from tkinter import filedialog, StringVar, ttk, BooleanVar, messagebox
from time import sleep
from Tools import ToPDF, checkFolderExist, PDFFormat, calculateHashes
from threading import Thread
from mail import mailanalyse
from database_msc import query
from Pdf import pdf_analyse
from datetime import datetime
import slugify
from contextlib import contextmanager
import sys, os

@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout

global lastRunDateTime

root = tk.Tk()
root.title("IPFIT7 Tool")
root.resizable(width=False, height=False)

# usable parameters
fileName = StringVar()
folderName = StringVar()
folderName.set("C:/Output")  # Set default output location
stabilityFolderName = StringVar()

caseNameI = StringVar()
caseDescI = []
InvestigatorI = StringVar()

dateDayS = StringVar()
dateMonthS = StringVar()
dateYearS = StringVar()
dateDayE = StringVar()
dateMonthE = StringVar()
dateYearE = StringVar()

dateDaySPdf = StringVar()
dateMonthSPdf = StringVar()
dateYearSPdf = StringVar()
dateDayEPdf = StringVar()
dateMonthEPdf = StringVar()
dateYearEPdf = StringVar()

keyword_1 = StringVar()
keyword_2 = StringVar()
keyword_3 = StringVar()

keyword_1S = StringVar()
keyword_2S = StringVar()
keyword_3S = StringVar()

dbServerI = StringVar()
dbNameI = StringVar()
dbUsernameI = StringVar()
dbPasswordI = StringVar()

# Checkboxes persons
optionBoxInformative = BooleanVar()

optionBoxWeightLoads = BooleanVar()
optionBoxWeightContainers = BooleanVar()
optionBoxWeightDWT = BooleanVar()

optionBoxCaptains = BooleanVar()
optionBoxContainers = BooleanVar()
optionBoxShips = BooleanVar()
optionBoxTerminalOperators = BooleanVar()
optionBoxVGM = BooleanVar()

checkHashes = BooleanVar()


# Should be used when creating a pdf file
def readCaseDesc():
    caseDescI.clear()
    caseDescI.append(caseDescInput.get("1.0", 'end-1c'))


def selectFolder():
    foldername = filedialog.askdirectory(initialdir="/", title="Select output directory")
    folderName.set(foldername)


def selectStabilityFolder():
    stabilityfoldername = filedialog.askdirectory(initialdir="/", title="Select the directory "
                                                                        "with all the container stowage in it")
    stabilityFolderName.set(stabilityfoldername)


def openFile():
    filename = filedialog.askopenfilename(initialdir="/", title="Select File",
                                          filetypes=(("mail files", "*.pst"), ("all files", "*.*"), ))
    fileName.set(filename)


def loading():
    with suppress_stdout():
        DBDict = {}
        DBDict['Informative'] = optionBoxInformative.get()
        DBDict['WeightLoads'] = optionBoxWeightLoads.get()
        DBDict['WeightContainers'] = optionBoxWeightContainers.get()
        DBDict['WeightDWT'] = optionBoxWeightDWT.get()
        DBDict['Captains'] = optionBoxCaptains.get()
        DBDict['Containers'] = optionBoxContainers.get()
        DBDict['Ships'] = optionBoxShips.get()
        # TODO Remon ik heb hier de naam aangepast
        DBDict['Terminal IDs'] = optionBoxTerminalOperators.get()
        DBDict['VGM'] = optionBoxVGM.get()

        mailKeywordList = [keyword_1.get(), keyword_2.get(), keyword_3.get()]
        startDateEmail = dateYearS.get() + '-' + dateMonthS.get() + '-' + dateDayS.get()
        EndDateEmail = dateYearE.get() + '-' + dateMonthE.get() + '-' + dateDayE.get()
        Mail = False
        PDF = False
        DB = False
        if fileName.get():
            Mail = True
        if stabilityFolderName.get():
            PDF = True

        if optionBoxInformative.get():
            DB = True
        elif optionBoxWeightLoads.get():
            DB = True
        elif optionBoxWeightContainers.get():
            DB = True
        elif optionBoxWeightDWT.get():
            DB = True
        elif optionBoxCaptains.get():
            DB = True
        elif optionBoxContainers.get():
            DB = True
        elif optionBoxShips.get():
            DB = True
        elif optionBoxTerminalOperators.get():
            DB = True
        elif optionBoxVGM.get():
            DB = True

        now = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        name = slugify.slugify(str(caseNameI.get()))
        if caseNameI.get() == "":
            name = "IPFIT7Case"
        global lastRunDateTime
        lastRunDateTime = name + '-' + now
        outputFolder = folderName.get() + '/' + name + '-' + now

        checkFolderExist.checkFolderExist(outputFolder)

        teams = range(60)
        if '.mbox' in fileName.get():
            teams = range(420)

        # start progress bar
        popup = tk.Toplevel()
        popup.resizable(width=False, height=False)
        tk.Label(popup, text="Process Running").grid(row=0, column=0, padx=10)

        progress = 0
        progress_var = tk.DoubleVar()
        progress_bar = ttk.Progressbar(popup, variable=progress_var, maximum=100, length=150)
        progress_bar.grid(row=1, column=0, pady=5, padx=30)  # .pack(fill=tk.X, expand=1, side=tk.BOTTOM)
        popup.pack_slaves()

        readCaseDesc()
        infoList = [caseNameI.get(), caseDescI[0], InvestigatorI.get(), str(Mail), str(PDF), str(DB)]
        formattedList = PDFFormat.info(infoList)
        ToPDF.ToPDFInfo(formattedList, outputFolder, "CaseInfo")

        if Mail is True:
            threadMail = Thread(target=mailanalyse.mailanalyse, args=(fileName.get(), outputFolder, mailKeywordList,
                                                                      startDateEmail, EndDateEmail))
            threadMail.start()
        if PDF is True:
            threadPdf = Thread(target=pdf_analyse.get_conplan_files,
                               args=(stabilityFolderName.get(),dateDaySPdf.get, dateMonthSPdf.get(),
                                          dateYearSPdf.get(), dateDayEPdf.get(),dateMonthEPdf.get(), dateYearEPdf,
                                          keyword_1S.get(), keyword_2S.get(), keyword_3S.get(), outputFolder))
            threadPdf.start()
        if DB is True:
            threadDb = Thread(target=query.main(outputFolder, DBDict))
            threadDb.start()

        progress_step = float(100.0 / len(teams))
        for team in teams:
            popup.update()
            sleep(0.2)  # lauch task
            progress += progress_step
            progress_var.set(progress)

        popup.destroy()
        popup.update()


def hash(outputFolderHash):
    hashLists = calculateHashes.main(outputFolderHash)
    hashtemplate = PDFFormat.hashes(hashLists)
    ToPDF.ToPDFHash(hashtemplate, outputFolderHash, 'Hashes')


def hashPopup():
    # start progress bar
    popup = tk.Toplevel()
    popup.resizable(width=False, height=False)
    tk.Label(popup, text="Process Running").grid(row=0, column=0, padx=10)
    progress = 0
    progress_var = tk.DoubleVar()
    progress_bar = ttk.Progressbar(popup, variable=progress_var, maximum=100, length=150)
    progress_bar.grid(row=1, column=0, pady=5, padx=30)  # .pack(fill=tk.X, expand=1, side=tk.BOTTOM)
    popup.pack_slaves()
    teams2 = range(20)
    progress_step = float(100.0 / len(teams2))
    for team in teams2:
        popup.update()
        sleep(0.2)  # lauch task
        progress += progress_step
        progress_var.set(progress)

    popup.destroy()
    popup.update()


def on_closing():
    if checkHashes.get():
        global lastRunDateTime
        if "lastRunDateTime" in globals():
            outputFolderHash = folderName.get() + '/' + lastRunDateTime
            if checkFolderExist.checkFolderExist(outputFolderHash, False):
                Thread(target=hash(outputFolderHash)).start()
                hashPopup()
    root.destroy()


# Creat
# ing tabs
tabControl = ttk.Notebook(root)

tabMain = ttk.Frame(tabControl)
tabControl.add(tabMain, text="Main")
tabControl.pack(expand=1, fill="both")

tabInfo = ttk.Frame(tabControl)
tabControl.add(tabInfo, text="Case Info")
tabControl.pack(expand=1, fill="both")

tabMail = ttk.Frame(tabControl)
tabControl.add(tabMail, text="Mail")
tabControl.pack(expand=1, fill="both")

tabDB = ttk.Frame(tabControl)
tabControl.add(tabDB, text="Database")
tabControl.pack(expand=1, fill="both")

tabStab = ttk.Frame(tabControl)
tabControl.add(tabStab, text="Container Plans")
tabControl.pack(expand=1, fill="both")

tabRun = ttk.Frame(tabControl)
tabControl.add(tabRun, text="Run")
tabControl.pack(expand=1, fill="both")

# Base layer tab 1
canvas1 = tk.Canvas(tabMain, height=400, width=700, bg="#263D42")
canvas1.pack()
frame1 = tk.Frame(tabMain, bg="white")
frame1.place(relwidth=0.9, relheight=0.9, relx=0.05, rely=0.05)

# label

labelmain2 = tk.Label(frame1,
                      text="IPFIT7 Tool\n\nRemon Hoogendijk\nRicardo Meijer\nMathijs Schouten",
                      bg="white",
                      relief="solid",
                      font="Times 15",
                      width=40,
                      height=10)
labelmain2.pack(pady=50) \
 \
# next page Button
buttonFrame1 = tk.Frame(frame1, bg="white")
buttonFrame1.place(relwidth=1, height=60, rely=0.8, )
run1 = tk.Button(buttonFrame1, text="Start", fg="white", bg="#263D42", height=20, width=15, font=40,
                 command=lambda: tabControl.select(tabInfo))
run1.pack(pady=12, anchor='c', padx='15')

# Base layer tab 2
canvas2 = tk.Canvas(tabMail, height=400, width=700, bg="#263D42")
canvas2.pack()
frame2 = tk.Frame(tabMail, bg="white")
frame2.place(relwidth=0.9, relheight=0.9, relx=0.05, rely=0.05)

# Open file row
tekstFrame = tk.Frame(frame2, bg="white")
tekstFrame.place(relwidth=1, height=35, rely=0.03)
openPts = tk.Button(tekstFrame, text="Open File", padx=10, pady=5, fg="white", bg="#263D42", command=openFile)
openPts.pack(anchor="w", padx=10)
label = tk.Label(frame2, width=80, textvariable=fileName, fg="black", relief="sunken")
label.pack(anchor="n", pady=15, padx=100)

# enter date row 1
dateFrame = tk.Frame(frame2, bg="white")
dateFrame.place(relwidth=1, height=50, rely=0.12)
startDate = tk.Label(dateFrame, width=10, text="Start Date:", fg="black", bg="white")
day = tk.Label(dateFrame, width=4, text="Day", fg="black", bg="white")
dayInput = tk.Entry(dateFrame, width=10, bg="whitesmoke", textvariable=dateDayS)
month = tk.Label(dateFrame, width=4, text="Month", fg="black", bg="white")
monthInput = tk.Entry(dateFrame, width=10, bg="whitesmoke", textvariable=dateMonthS)
year = tk.Label(dateFrame, width=4, text="Year", fg="black", bg="white")
yearInput = tk.Entry(dateFrame, width=10, bg="whitesmoke", textvariable=dateYearS)

startDate.pack(side=tk.LEFT, padx=10)
day.pack(side=tk.LEFT, padx=10)
dayInput.pack(side=tk.LEFT)
month.pack(side=tk.LEFT, padx=10)
monthInput.pack(side=tk.LEFT)
year.pack(side=tk.LEFT, padx=10)
yearInput.pack(side=tk.LEFT)

# enter date row 2
dateFrame2 = tk.Frame(frame2, bg="white")
dateFrame2.place(relwidth=1, height=30, rely=0.22)
endDate = tk.Label(dateFrame2, width=10, text="End Date:", fg="black", bg="white")
day2 = tk.Label(dateFrame2, width=4, text="Day", fg="black", bg="white")
dayInput2 = tk.Entry(dateFrame2, width=10, bg="whitesmoke", textvariable=dateDayE)
month2 = tk.Label(dateFrame2, width=4, text="Month", fg="black", bg="white")
monthInput2 = tk.Entry(dateFrame2, width=10, bg="whitesmoke", textvariable=dateMonthE)
year2 = tk.Label(dateFrame2, width=4, text="Year", fg="black", bg="white")
yearInput2 = tk.Entry(dateFrame2, width=10, bg="whitesmoke", textvariable=dateYearE)

endDate.pack(side=tk.LEFT, padx=10)
day2.pack(side=tk.LEFT, padx=10)
dayInput2.pack(side=tk.LEFT)
month2.pack(side=tk.LEFT, padx=10)
monthInput2.pack(side=tk.LEFT)
year2.pack(side=tk.LEFT, padx=10)
yearInput2.pack(side=tk.LEFT)

# keyword parameter row #1
keywordFrame = tk.Frame(frame2, bg="white")
keywordFrame.place(relwidth=1, height=50, rely=0.36)
keyword = tk.Label(keywordFrame, width=12, text="keyword #1", fg="black", bg="white")
keywordInput = tk.Entry(keywordFrame, width=73, bg="whitesmoke", textvariable=keyword_1)
keyword.pack(side=tk.LEFT)
keywordInput.pack(side=tk.LEFT)

# keyword parameter row #2
keywordFrame2 = tk.Frame(frame2, bg="white")
keywordFrame2.place(relwidth=1, height=50, rely=0.46)
keyword2 = tk.Label(keywordFrame2, width=12, text="keyword #2", fg="black", bg="white")
keywordInput2 = tk.Entry(keywordFrame2, width=73, bg="whitesmoke", textvariable=keyword_2)
keyword2.pack(side=tk.LEFT)
keywordInput2.pack(side=tk.LEFT)

# keyword parameter row #3
keywordFrame3 = tk.Frame(frame2, bg="white")
keywordFrame3.place(relwidth=1, height=50, rely=0.56)
keyword3 = tk.Label(keywordFrame3, width=12, text="keyword #3", fg="black", bg="white")
keywordInput3 = tk.Entry(keywordFrame3, width=73, bg="whitesmoke", textvariable=keyword_3)
keyword3.pack(side=tk.LEFT)
keywordInput3.pack(side=tk.LEFT)

# next page Button
buttonFrame2 = tk.Frame(frame2, bg="white")
buttonFrame2.place(relwidth=1, height=60, rely=0.8, )
run2 = tk.Button(buttonFrame2, text="Next >>>", fg="white", bg="#263D42", height=20, width=15, font=40,
                 command=lambda: tabControl.select(tabDB))
run2.pack(pady=12, anchor='e', padx='15')

# Base layer tab 3
canvas3 = tk.Canvas(tabDB, height=400, width=700, bg="#263D42")
canvas3.pack()
frame3 = tk.Frame(tabDB, bg="white")
frame3.place(relwidth=0.9, relheight=0.9, relx=0.05, rely=0.05)

labelDB = tk.Label(frame3,
                   text="Database",
                   bg="white",
                   font="Times 24", )
labelDB.pack(pady=15)

optionInformative = tk.Checkbutton(frame3, bg='white', text="Export DB tables in PDF Format", variable=optionBoxInformative,
                                   onvalue=True, offvalue=False)
optionInformative.pack()

frame31 = tk.Frame(frame3, bg="white")
frame31.place(rely=0.28, relwidth=1, relheight=0.7)

labelDB2 = tk.Label(frame3,
                    text="Missing Data Audit                                                     Weight Audit",
                    bg="white",
                    font="Times 12", )
labelDB2.pack(pady=15)

# Frame person with options
MissingDataFrame = tk.Frame(frame31, bg="white", )
MissingDataFrame.pack(side=tk.LEFT, expand=True, fill=tk.X, anchor='n', padx=62, pady=50)
optionCaptains = tk.Checkbutton(MissingDataFrame, bg='white', text="Shippings Without Captain", variable=optionBoxCaptains, onvalue=True,
                                offvalue=False)
optionContainers = tk.Checkbutton(MissingDataFrame, bg='white', text="Containers Without Payload Weight", variable=optionBoxContainers,
                                  onvalue=True, offvalue=False)
optionShips = tk.Checkbutton(MissingDataFrame, bg='white', text="Shippings Without Ship", variable=optionBoxShips, onvalue=True,
                             offvalue=False)
optionTerminalOperators = tk.Checkbutton(MissingDataFrame, bg='white', text="Shippings Without (un)loading Terminal",
                                         variable=optionBoxTerminalOperators, onvalue=True, offvalue=False)
optionVGM = tk.Checkbutton(MissingDataFrame, bg='white', text="Shippings Without VGM", variable=optionBoxVGM, onvalue=True,
                           offvalue=False)

optionCaptains.grid(row=0, column=1, columnspan=1, sticky="w")
optionContainers.grid(row=1, column=1, columnspan=1, sticky="w")
optionShips.grid(row=2, column=1, columnspan=1, sticky="w")
optionTerminalOperators.grid(row=3, column=1, columnspan=1, sticky="w")
optionVGM.grid(row=4, column=1, columnspan=1, sticky="w")

# Frame cargo with options
cargoFrame = tk.Frame(frame31, bg="white")
cargoFrame.pack(side=tk.LEFT, expand=True, fill=tk.X, anchor='n', padx=20, pady=50)
optionWeightContainers = tk.Checkbutton(cargoFrame, bg='white', text="Overloaded Containers",
                                        variable=optionBoxWeightContainers, onvalue=True, offvalue=False)
optionWeightDWT = tk.Checkbutton(cargoFrame, bg='white', text="Overloaded Shippings", variable=optionBoxWeightDWT,
                                 onvalue=True, offvalue=False)
optionWeightLoads = tk.Checkbutton(cargoFrame, bg='white', text="Overloaded Loads", variable=optionBoxWeightLoads,
                                   onvalue=True, offvalue=False)

optionWeightContainers.pack(anchor='w')
optionWeightDWT.pack(anchor='w')
optionWeightLoads.pack(anchor='w')

# next page Button
buttonFrame3 = tk.Frame(frame3, bg="white")
buttonFrame3.place(relwidth=1, height=60, rely=0.8, )
run3 = tk.Button(buttonFrame3, text="Next >>>", fg="white", bg="#263D42", height=20, width=15, font=40,
                 command=lambda: tabControl.select(tabStab))
run3.pack(pady=12, anchor='e', padx='15')

## Base layer tab 5
canvas5 = tk.Canvas(tabStab, height=400, width=700, bg="#263D42")
canvas5.pack()
frame5 = tk.Frame(tabStab, bg="white")
frame5.place(relwidth=0.9, relheight=0.9, relx=0.05, rely=0.05)

## Stability calculations tab
# Select folder row
stabilityFolderFrame = tk.Frame(frame5, bg="white")
stabilityFolderFrame.place(relwidth=1, height=35, rely=0.03)
selectFolderStab = tk.Button(stabilityFolderFrame, text="Select Folder", padx=10, pady=5, fg="white", bg="#263D42",
                             command=selectStabilityFolder)
selectFolderStab.pack(anchor="w", padx=10)
label = tk.Label(frame5, width=80, textvariable=stabilityFolderName, fg="black", relief="sunken")
label.pack(anchor="n", pady=15, padx=120)
# Start date
dateFrameSts = tk.Frame(frame5, bg="white")
dateFrameSts.place(relwidth=1, height=50, rely=0.12)
startDateSts = tk.Label(dateFrameSts, width=10, text="Start Date:", fg="black", bg="white")
daySts = tk.Label(dateFrameSts, width=4, text="Day", fg="black", bg="white")
dayInputSts = tk.Entry(dateFrameSts, width=10, bg="whitesmoke", textvariable=dateDaySPdf)
monthSts = tk.Label(dateFrameSts, width=4, text="Month", fg="black", bg="white")
monthInputSts = tk.Entry(dateFrameSts, width=10, bg="whitesmoke", textvariable=dateMonthSPdf)
yearSts = tk.Label(dateFrameSts, width=4, text="Year", fg="black", bg="white")
yearInputSts = tk.Entry(dateFrameSts, width=10, bg="whitesmoke", textvariable=dateYearSPdf)

startDateSts.pack(side=tk.LEFT, padx=10)
daySts.pack(side=tk.LEFT, padx=10)
dayInputSts.pack(side=tk.LEFT)
monthSts.pack(side=tk.LEFT, padx=10)
monthInputSts.pack(side=tk.LEFT)
yearSts.pack(side=tk.LEFT, padx=10)
yearInputSts.pack(side=tk.LEFT)
# End date
dateFrameSte = tk.Frame(frame5, bg="white")
dateFrameSte.place(relwidth=1, height=30, rely=0.22)
endDateSte = tk.Label(dateFrameSte, width=10, text="End Date:", fg="black", bg="white")
daySte = tk.Label(dateFrameSte, width=4, text="Day", fg="black", bg="white")
dayInputSte = tk.Entry(dateFrameSte, width=10, bg="whitesmoke", textvariable=dateDayEPdf)
monthSte = tk.Label(dateFrameSte, width=4, text="Month", fg="black", bg="white")
monthInputSte = tk.Entry(dateFrameSte, width=10, bg="whitesmoke", textvariable=dateMonthEPdf)
yearSte = tk.Label(dateFrameSte, width=4, text="Year", fg="black", bg="white")
yearInputSte = tk.Entry(dateFrameSte, width=10, bg="whitesmoke", textvariable=dateYearEPdf)

endDateSte.pack(side=tk.LEFT, padx=10)
daySte.pack(side=tk.LEFT, padx=10)
dayInputSte.pack(side=tk.LEFT)
monthSte.pack(side=tk.LEFT, padx=10)
monthInputSte.pack(side=tk.LEFT)
yearSte.pack(side=tk.LEFT, padx=10)
yearInputSte.pack(side=tk.LEFT)
# Keyword fields
keywordFrame = tk.Frame(frame5, bg="white")
keywordFrame.place(relwidth=1, height=50, rely=0.36)
keyword = tk.Label(keywordFrame, width=12, text="keyword #1", fg="black", bg="white")
keywordInput = tk.Entry(keywordFrame, width=73, bg="whitesmoke", textvariable=keyword_1S)
keyword.pack(side=tk.LEFT)
keywordInput.pack(side=tk.LEFT)

keywordFrame2 = tk.Frame(frame5, bg="white")
keywordFrame2.place(relwidth=1, height=50, rely=0.46)
keyword2 = tk.Label(keywordFrame2, width=12, text="keyword #2", fg="black", bg="white")
keywordInput2 = tk.Entry(keywordFrame2, width=73, bg="whitesmoke", textvariable=keyword_2S)
keyword2.pack(side=tk.LEFT)
keywordInput2.pack(side=tk.LEFT)

keywordFrame3 = tk.Frame(frame5, bg="white")
keywordFrame3.place(relwidth=1, height=50, rely=0.56)
keyword3 = tk.Label(keywordFrame3, width=12, text="keyword #3", fg="black", bg="white")
keywordInput3 = tk.Entry(keywordFrame3, width=73, bg="whitesmoke", textvariable=keyword_3S)
keyword3.pack(side=tk.LEFT)
keywordInput3.pack(side=tk.LEFT)

# next page Button
buttonFrame5 = tk.Frame(frame5, bg="white")
buttonFrame5.place(relwidth=1, height=60, rely=0.8, )
run5 = tk.Button(buttonFrame5, text="Next >>>", fg="white", bg="#263D42", height=20, width=15, font=40,
                 command=lambda: tabControl.select(tabRun))
run5.pack(pady=12, anchor='e', padx='15')

# Base layer info tab
canvas4 = tk.Canvas(tabInfo, height=400, width=700, bg="#263D42")
canvas4.pack()
frame4 = tk.Frame(tabInfo, bg="white")
frame4.place(relwidth=0.9, relheight=0.9, relx=0.05, rely=0.05)

# select output folder
outputFolderFrame = tk.Frame(frame4, bg="white")
outputFolderFrame.place(relwidth=1, height=35, rely=0.03)
outputFolder = tk.Button(outputFolderFrame, text="Output Folder", padx=10, pady=5, fg="white", bg="#263D42",
                         command=selectFolder)
outputFolder.pack(anchor="w", padx=10)
label = tk.Label(frame4, width=60, textvariable=folderName, fg="black", relief="sunken")
label.pack(anchor="e", pady=15, padx=60)

# Case name
caseNameFrame = tk.Frame(frame4, bg="white")
caseNameFrame.place(relwidth=1, height=35, rely=0.14)
caseName = tk.Label(caseNameFrame, width=12, text="Case name", fg="black", bg="white")
caseNameInput = tk.Entry(caseNameFrame, width=70, bg="whitesmoke", textvariable=caseNameI)
caseName.pack(side=tk.LEFT, padx=0)
caseNameInput.pack(side=tk.LEFT, padx=60)

# Case Description
caseDescFrame = tk.Frame(frame4, bg="white")
caseDescFrame.place(relwidth=1, height=60, rely=0.24)
caseDesc = tk.Label(caseDescFrame, width=12, text="Case Description", fg="black", bg="white")
caseDescInput = tk.Text(caseDescFrame, width=52, bg="whitesmoke")
caseDesc.pack(side=tk.LEFT, padx=15)
caseDescInput.pack(side=tk.LEFT, padx=30)

# researcher name
InvestigatorFrame = tk.Frame(frame4, bg="white")
InvestigatorFrame.place(relwidth=1, height=35, rely=0.44)
Investigator = tk.Label(InvestigatorFrame, width=12, text="Investigator", fg="black", bg="white")
InvestigatorInput = tk.Entry(InvestigatorFrame, width=70, bg="whitesmoke", textvariable=InvestigatorI)
Investigator.pack(side=tk.LEFT, padx=2)
InvestigatorInput.pack(side=tk.LEFT, padx=55)

# next page Button
buttonFrame4 = tk.Frame(frame4, bg="white")
buttonFrame4.place(relwidth=1, height=60, rely=0.8, )
run4 = tk.Button(buttonFrame4, text="Next >>>", fg="white", bg="#263D42", height=20, width=15, font=40,
                 command=lambda: tabControl.select(tabMail))
run4.pack(pady=12, anchor='e', padx='15')

# Base layer run tab
canvasRun = tk.Canvas(tabRun, height=400, width=700, bg="#263D42")
canvasRun.pack()
frameRun = tk.Frame(tabRun, bg="white")
frameRun.place(relwidth=0.9, relheight=0.9, relx=0.05, rely=0.05)

# RUN Button
buttonFrameRun = tk.Frame(frameRun, bg="white")
buttonFrameRun.place(relwidth=1, height=100, rely=0.22)
run = tk.Button(buttonFrameRun, text="RUN", fg="white", bg="#263D42", height=20, width=20, font="40", command=loading)
run.pack(pady=12)

optionHashes = tk.Checkbutton(frameRun, bg='white', text="Calculate Hashes upon closing", variable=checkHashes,
                              onvalue=True, offvalue=False)
optionHashes.pack(side=tk.BOTTOM, pady=150)

root.protocol("WM_DELETE_WINDOW", on_closing)
root.mainloop()
