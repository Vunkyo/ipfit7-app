from tika import parser
import os
from datetime import date as datemod, timedelta
import shutil
from Tools import checkFolderExist


def check_keywords(out, file, keyword1, keyword2, keyword3, folderName, filepath):
    if keyword1 != "" and keyword2 == "":
        if any(keyword1.lower() in s.lower() for s in out):
            checkFolderExist.checkFolderExist(folderName + '/' + "ContainerPlans_" + keyword1)
            shutil.copy2(filepath, folderName + '/' + "ContainerPlans_" + keyword1 + "/" + file)
    elif keyword1 != "" and keyword2 != "" and keyword3 == "":
        if any(keyword1.lower() in s.lower() for s in out) and any(keyword2.lower() in s.lower() for s in out):
            checkFolderExist.checkFolderExist(folderName + '/' + "ContainerPlans_" + keyword1 + '_' + keyword2)
            shutil.copy2(filepath, folderName + '/' + "ContainerPlans_" + keyword1 + '_' + keyword2 + "/" + file)
    elif keyword1 != "" and keyword2 != "" and keyword3 != "":
        if any(keyword1.lower() in s.lower() for s in out) and any(keyword2.lower() in s.lower() for s in out) and any(keyword3.lower() in s.lower() for s in out):
            checkFolderExist.checkFolderExist(folderName + '/' + "ContainerPlans_" + keyword1 + '_' + keyword2 + '_' + keyword3)
            shutil.copy2(filepath,
                         folderName + '/' + "ContainerPlans_" + keyword1 + '_' + keyword2 + '_' + keyword3 + "/" + file)
    else:
        checkFolderExist.checkFolderExist(folderName + '/' + "ContainerPlans")
        shutil.copy2(filepath, folderName + '/' + "ContainerPlans/" + file)


def get_conplan_files(folderpath, startday, startmonth, startyear, endday,
                        endmonth, endyear, keyword1, keyword2, keyword3, folderName):

    if len(startyear) == 4:
        sdate = datemod(int(startyear), int(startmonth), int(startday))
        edate = datemod(int(endyear), int(endmonth), int(endday))
        delta = edate - sdate

    for foldername, subfolders, files in os.walk(str(folderpath)):
        for file in files:
            filepath = foldername + '/' + file
            raw = parser.from_file(filepath)
            out = []
            buff = []
            for c in raw['content']:
                if c == '\n':
                    out.append(''.join(buff))
                    buff = []
                else:
                    buff.append(c)
            else:
                if buff:
                    out.append(''.join(buff))

            out = list(filter(None, out))

            if len(startyear) == 4:
                for i in range(delta.days + 1):
                    day = sdate + timedelta(days=i)
                    if len(str(day.month)) == 1:
                        strmonth = "0" + str(day.month)
                    else: strmonth = str(day.mont)
                    if len(str(day.day)) == 1:
                        strday = "0" + str(day.day)
                    else: strday = str(day.day)
                    date = strday + "-" + strmonth + "-" + str(day.year)
                    if any(date in s for s in out):
                        check_keywords(out, file, keyword1, keyword2, keyword3, folderName, filepath)
            else: check_keywords(out, file, keyword1, keyword2, keyword3, folderName, filepath)
