import os

def checkFolderExist(dirName, createNew=True):
    # Create target directory & all intermediate directories if don't exists
    if not os.path.exists(dirName):
        if createNew is True:
            os.makedirs(dirName)
            print("Directory ", dirName, " Created ")
        else:
            print("Directory ", dirName, " does not exist")
        return False
    else:
        print("Directory ", dirName, " already exists")
        return True