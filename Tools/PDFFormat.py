import itertools


def loopAttachments(item):
    string = "Attachments: "
    for key in item:
        if 'file' in key:
            string += " - " + str(item[key][0])
    if '-' not in string:
        string += 'None'

    return string


def mailTemplate(mailList):
    templateList = []
    for mail in mailList:
        tinyList = []
        tinyList.append("From: " + str(mail['From']))
        tinyList.append("To: " + str(mail['To']))
        if mail['Date'] is None:
            tinyList.append("Date: None")
        else:
            tinyList.append("Date: " + str(mail['Date'][:-5]))
        tinyList.append("Subject: " + str(mail['Subject']))
        tinyList.append(loopAttachments(mail))
        tinyList.append("Body:")
        tinyList.append("")
        tinyList.append(str(mail['Body']))
        templateList.append(tinyList.copy())

    templateList.sort()
    templateList = list(templateList for templateList, _ in itertools.groupby(templateList))
    return templateList


def captains(captains):
    templateList = ["ID | Name | E-mail | Telephone | Crew ID"]
    for captain in captains:
        templateList.append(
            str(captain[0]) + " | " + str(captain[1]) + " | " + str(captain[2]) + " | " + str(captain[3]) + " | " + str(
                captain[4]))

    return templateList


def captain_crews(crews):
    templateList = ["ID | Names"]
    for crew in crews:
        templateList.append(str(crew[0]) + " | " + str(crew[1]))

    return templateList


def containers(containers):
    templateList = ["ID | Tare Weight | Payload Weight | Max Payload | Max Gross | Cubic Meters"]
    for container in containers:
        templateList.append(
            str(container[0]) + " | " + str(container[1]) + " | " + str(container[2]) + " | " + str(
                container[3]) + " | " + str(
                container[4]) + " | " + str(container[4]))

    return templateList


def overloaded_shippings(data):
    templateList = ["ID | VGM Authority | Total Weight | Ship DWT | Difference"]
    for item in data:
        total_weight = item[2]
        ship_dwt = item[3]
        difference = total_weight - ship_dwt
        templateList.append(
            str(item[0]) + " | " + str(item[1]) + " | " + str(item[2]) + " | " + str(item[3]) + " | " + str(difference))

    return templateList


def harbors(harbors):
    templateList = ["ID | Name | Country | Terminal ID's"]
    for harbor in harbors:
        templateList.append(str(harbor[0]) + " | " + str(harbor[1]) + " | " + str(harbor[2]) + " | " + str(harbor[3]))

    return templateList


def loads(loads):
    templateList = ["ID | Container ID's | Cumulative Weight | Total Weight"]
    for load in loads:
        templateList.append(str(load[0]) + " | " + str(load[1]) + " | " + str(load[2]) + " | " + str(load[3]))

    return templateList


def shippings(shippings):
    templateList = ["ID | Load ID | Departure Date | Arrival Date | Departure Harbor ID | Arrival Harbor ID | Ship ID "
                    "| Captain ID | VGM Authority ID | Loading Terminal ID | Unloading Terminal ID | "]
    for shipping in shippings:
        templateList.append(
            str(shipping[0]) + " | " + str(shipping[1]) + " | " + str(shipping[2]) + " | " + str(shipping[3]) + " | " +
            str(shipping[4]) + " | " + str(shipping[5]) + " | " + str(shipping[6]) + " | " + str(shipping[7]) + " | " +
            str(shipping[8]) + " | " + str(shipping[9]) + " | " + str(shipping[10])
        )

    return templateList


def ships(ships):
    templateList = ["ID | Name | Ocean Carrier | Deadweight Tonnage (DWT)"]
    for ship in ships:
        templateList.append(str(ship[0]) + " | " + str(ship[1]) + " | " + str(ship[2]) + " | " + str(ship[3]))

    return templateList


def terminal_operators(terminal_operators):
    templateList = ["ID | Name | E-mail | Telephone"]
    for terminal_operator in terminal_operators:
        templateList.append(str(terminal_operator[0]) + " | " + str(terminal_operator[1]) + " | " + str(
            terminal_operator[2]) + " | " + str(terminal_operator[3]))

    return templateList


def vgm_authorities(vgm_authorities):
    templateList = ["ID | Name | E-mail | Telephone"]
    for vgm_authority in vgm_authorities:
        templateList.append(
            str(vgm_authority[0]) + " | " + str(vgm_authority[1]) + " | " + str(vgm_authority[2]) + " | " + str(
                vgm_authority[3]))

    return templateList


def info(infoList):
    templateList = []
    templateList.append("Case Name: " + infoList[0])
    templateList.append("Case Description: ")
    templateList.append(infoList[1])
    templateList.append("")
    templateList.append("Investigator: " + infoList[2])
    templateList.append("")
    templateList.append("Performed Extractions")
    templateList.append("========================================")
    templateList.append("Mail: " + infoList[3])
    templateList.append("Pdf: " + infoList[4])
    templateList.append("DB: " + infoList[5])

    return templateList


# hashLists should look like
# [
#     [filename, md5, sha1],
#     [filename, md5, sha1],
#     [filename, md5, sha1]
# ]
def hashes(hashLists):
    templateList = []
    for list in hashLists:
        if list:
            templateList.append("filename: " + str(list[0]))
            templateList.append("========================================================")
            templateList.append("MD5: " + str(list[1]))
            templateList.append("SHA1: " + str(list[2]))
            templateList.append("SHA256: " + str(list[3]))
            templateList.append("")
    return templateList
