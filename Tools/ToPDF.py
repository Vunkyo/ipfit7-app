import fpdf
import re
from datetime import datetime
from Tools import checkFolderExist


def pdf(list, Dir, i, naming):
    pdf = fpdf.FPDF()
    pdf.add_page()
    pdf.set_font("Arial", size=11)
    for string in list:
        pdf.multi_cell(200, 6, txt=str(string.encode('utf-8'))[2:-1], align='L')
    pdf.output(Dir + naming + str(i) + '.pdf', dest='F')


def ToPDF(listList, outputDir, naming):
    Dir = outputDir
    checkFolderExist.checkFolderExist(Dir)
    i = 0
    for list in listList:
        pdf(list, Dir, i, naming)
        i += 1


def ToPDFDb(listList, outputDir, naming, ):
    checkFolderExist.checkFolderExist(outputDir)
    i = 0
    pdf = fpdf.FPDF()
    pdf.add_page()
    for string in listList:
        pdf.set_font("Arial", size=11)
        pdf.multi_cell(200, 6, txt=str(string.encode('utf-8'))[2:-1], align='L')
    pdf.output(outputDir + naming + '.pdf', dest='F')


def ToPDFInfo(InfoList, outputDir, naming):
    checkFolderExist.checkFolderExist(outputDir)
    pdf = fpdf.FPDF()
    pdf.add_page()
    for string in InfoList:
        pdf.set_font("Arial", size=11)
        pdf.multi_cell(200, 6, txt=str(string.encode('utf-8'))[2:-1], align='L')
    pdf.output(outputDir + '/' + naming + '.pdf', dest='F')


def ToPDFHash(HashList, outputDir, naming):
    pdf = fpdf.FPDF()
    pdf.add_page()
    for string in HashList:
        pdf.set_font("Arial", size=11)
        pdf.multi_cell(200, 6, txt=str(string.encode('utf-8'))[2:-1], align='L')
    pdf.output(outputDir + '/' + naming + '.pdf', dest='F')