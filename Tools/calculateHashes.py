import os
import hashlib

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(2 ** 20), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def sha1(fname):
    hash_sha1 = hashlib.sha1()
    with open(fname, "rb") as f:
        for chuck in iter(lambda: f.read(2 ** 20), b""):
            hash_sha1.update(chuck)
    return hash_sha1.hexdigest()


def sha256(fname):
    hash_sha256 = hashlib.sha256()
    with open(fname, "rb") as f:
        for chuck in iter(lambda: f.read(2 ** 20), b""):
            hash_sha256.update(chuck)
    return hash_sha256.hexdigest()


def main(directory='./'):
    hashList = []
    for root, dirs, files in os.walk(directory):
        for names in files:
            filepath = os.path.join(root,names)
            hashList.append([filepath, md5(filepath), sha1(filepath), sha256(filepath)])
    return hashList