def InfoToText(infoList, outputFolder):
    with open(outputFolder.get() + "/output.txt", "w") as file:
        for item in infoList:
            file.write(item + "\n")
