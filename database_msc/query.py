"""
Copyright (C) IT-Everywhere

This file is part of ipfit7-app.

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

Written by Mathijs Schouten <contact@it-everywhere.nl> - IT-Everywhere, June 2020
"""

from database_msc.databaseConnectionClass import *
from Tools import ToPDF, PDFFormat


def main(outputDir, DBDict):
    # Creating DB Connection
    dbConnection = Database_Connection()

    print(DBDict)

    if DBDict['Informative']:
        # ======= BEGIN INFORMATIVE EXPORTS =======

        # All Captains
        allCaptains = dbConnection.selectAllFromTable('captains')
        formattedList = PDFFormat.captains(allCaptains)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Informative/", "Captains")

        # All Captain Crews
        allCaptainCrews = dbConnection.selectAllFromTable('captain_crews')
        formattedList = PDFFormat.captain_crews(allCaptainCrews)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Informative/", "Captain Crews")

        # All Containers
        allContainers = dbConnection.selectAllFromTable('containers')
        formattedList = PDFFormat.containers(allContainers)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Informative/", "Containers")

        # All Harbors
        allHarbors = dbConnection.selectAllFromTable('harbors')
        formattedList = PDFFormat.harbors(allHarbors)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Informative/", "Harbors")

        # All Loads
        allLoads = dbConnection.selectAllFromTable('loads')
        formattedList = PDFFormat.loads(allLoads)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Informative/", "Loads")

        # All Shippings
        allShippings = dbConnection.selectAllFromTable('shippings')
        formattedList = PDFFormat.shippings(allShippings)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Informative/", "Shippings")

        # All Ships
        allShips = dbConnection.selectAllFromTable('ships')
        formattedList = PDFFormat.ships(allShips)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Informative/", "Ships")

        # All Terminal Operators
        allTerminalOperators = dbConnection.selectAllFromTable('terminal_operators')
        formattedList = PDFFormat.terminal_operators(allTerminalOperators)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Informative/", "Terminal Operators")

        # All VGM Authorities
        allVGMAuthorities = dbConnection.selectAllFromTable('vgm_authorities')
        formattedList = PDFFormat.vgm_authorities(allVGMAuthorities)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Informative/", "VGM Authorities")

        # ======= END INFORMATIVE EXPORTS =======

    # ======= BEGIN AUDIT EXPORTS =======

    # ----- WEIGHT -----

    # Loads with cumulative_weight > total_weight
    if DBDict['WeightLoads']:
        allOverloadedLoads = dbConnection.selectWhereFromTable('loads', 'cumulative_weight > total_weight')
        formattedList = PDFFormat.loads(allOverloadedLoads)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Audit/Weight/", "Overloaded Loads")

    # Shippings with loads.total_weight > ship.dwt
    if DBDict['WeightDWT']:
        allOverloadedShippings = dbConnection.selectAllFromTable('overloaded_shippings')
        formattedList = PDFFormat.overloaded_shippings(allOverloadedShippings)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Audit/Weight/", "Overloaded Shippings")

    # Containers with payload_weight > max_payload
    if DBDict['WeightContainers']:
        allOverloadedContainers = dbConnection.selectWhereFromTable('containers', 'payload_weight > max_payload')
        formattedList = PDFFormat.containers(allOverloadedContainers)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Audit/Weight/", "Overloaded Containers")

    # ----- MISSING DATA -----
    # Containers with payload_weight missing
    if DBDict['Containers']:
        allContainersWithoutPayloadWeight = dbConnection.selectWhereFromTable('containers', 'payload_weight = 0')
        formattedList = PDFFormat.containers(allContainersWithoutPayloadWeight)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Audit/Missing Data/Containers/", "Containers Without Payload Weight")

    # Shippings with vgm_authority missing
    if DBDict['VGM']:
        allShippingsWithoutVGM = dbConnection.selectWhereFromTable('shippings', 'vgm_authority_id = 0')
        formattedList = PDFFormat.shippings(allShippingsWithoutVGM)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Audit/Missing Data/Shippings/", "Shippings Without VGM")

    # Shippings with ship_id missing
    if DBDict['Ships']:
        allShippingsWithoutShip = dbConnection.selectWhereFromTable('shippings', 'ship_id = 0')
        formattedList = PDFFormat.shippings(allShippingsWithoutShip)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Audit/Missing Data/Shippings/", "Shippings Without Ship")

    # Shippings with captain_id missing
    if DBDict['Captains']:
        allShippingsWithoutCaptains = dbConnection.selectWhereFromTable('shippings', 'captain_id = 0')
        formattedList = PDFFormat.shippings(allShippingsWithoutCaptains)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Audit/Missing Data/Shippings/", "Shippings Without Captain")

    if DBDict['Terminal IDs']:
        # Shippings with loading_terminal_id missing
        allShippingsWithoutLoadingTerminal = dbConnection.selectWhereFromTable('shippings', 'loading_terminal_id = 0')
        formattedList = PDFFormat.shippings(allShippingsWithoutLoadingTerminal)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Audit/Missing Data/Shippings/", "Shippings Without Loading Terminal")

        # Shippings with unloading_terminal_id missing
        allShippingsWithoutUnloadingTerminal = dbConnection.selectWhereFromTable('shippings',
                                                                                 'unloading_terminal_id = 0')
        formattedList = PDFFormat.shippings(allShippingsWithoutUnloadingTerminal)
        ToPDF.ToPDFDb(formattedList, outputDir + "/Audit/Missing Data/Shippings/",
                      "Shippings Without Unloading Terminal")

    # ======= END AUDIT EXPORTS =======


if __name__ == '__main__':
    main(outputDir="/output/2020-06-14_12-40-23")
