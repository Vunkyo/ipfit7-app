"""
Copyright (C) IT-Everywhere

This file is part of ipfit7-app.

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

Written by Mathijs Schouten <contact@it-everywhere.nl> - IT-Everywhere, June 2020
"""

import mysql.connector
from mysql.connector import errorcode
from pathlib import Path


# Class for DB connection
class Database_Connection:

    def __init__(self):
        folderPath = Path(__file__).parents[0]
        self.credentialFileToOpen = str(folderPath) + "\\%s" % "credentials"

        with open(self.credentialFileToOpen, 'r') as databaseCredentials:
            credential = databaseCredentials.read().splitlines()
            self.user = (credential[0])
            self.password = (credential[1])
            self.host = (credential[2])
            self.database = (credential[3])

    def getConnector(self):
        try:
            connector = mysql.connector.connect(user=self.user, password=self.password,
                                                host=self.host, database=self.database)

            # print('Database connection was successfully')
            return connector

        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)

    def selectAllFromTable(self, tableName):
        connector = self.getConnector()
        cursor = connector.cursor()
        query = "SELECT * FROM %s" % tableName
        cursor.execute(query)
        data = cursor.fetchall()
        cursor.close()
        return data

    def selectWhereFromTable(self, tableName, whereClause):
        connector = self.getConnector()
        cursor = connector.cursor()
        query = "SELECT * FROM %s WHERE %s" % (tableName, whereClause)
        cursor.execute(query)
        data = cursor.fetchall()
        cursor.close()
        return data

