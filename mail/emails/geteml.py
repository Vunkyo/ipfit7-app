import email
import os
import html2text


def retreive_email(email_message):
    msgDict = {}


    try:
        i = 1
        for part in email_message.walk():
            if part.get_content_maintype() == 'multipart': continue
            if part.get('Content-Disposition') is None: continue
            filename = part.get_filename()
            if filename is not None:
                msgDict['file' + str(i)] = [filename, part.get_payload(decode=True)]
                i = i + 1


        if email_message.is_multipart():
            # print("multi")
            for payload in email_message.get_payload():
                # print('To:\t\t', email_message['To'])
                msgDict['To'] = email_message['To']
                # print('From:\t',     email_message['From'])
                msgDict['From'] = email_message['From']
                # print('Subject:', email_message['Subject'])
                msgDict['Subject'] = email_message['Subject']
                # print('Date:\t',email_message['Date'])
                msgDict['Date'] = email_message['Date']
                for part in email_message.walk():
                    # print("=================")
                    if (part.get_content_type() == 'text/plain') and (part.get('Content-Disposition') is None):
                        content = ''.join(str(part.get_payload()))
                    elif (part.get_content_type() == 'text/html'):
                        content = ''.join(str(html2text.html2text(part.get_payload(), 'lxml')))
                msgDict['Body'] = content
                # print(content)
                break
        else:
            # print("not multi")
            # print('To:\t\t', email_message['To'])
            msgDict['To'] = email_message['To']
            # print('From:\t', email_message['From'])
            msgDict['From'] = email_message['From']
            # print('Subject:', email_message['Subject'])
            msgDict['Subject'] = email_message['Subject']
            # print('Date:\t', email_message['Date'])
            msgDict['Date'] = email_message['Date']
            output = html2text.html2text(email_message.get_payload(), 'lxml')
            # print(output)
            msgDict['Body'] = output

    except IndexError:
        print("No email found")

    return msgDict

if __name__ == '__main__':
    with open("../Test eml's/simple.eml") as file:
        email_message = email.message_from_file(file)
        retreive_email(email_message)
        file.close()
