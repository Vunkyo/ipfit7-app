from libratom.lib.pff import PffArchive
from pathlib import Path
import email
from mail.emails import geteml

def retrieve_email(file):
    emailList = []
    filePath = Path(file)
    archive = PffArchive(filePath)

    for folder in archive.folders():
        if folder.get_number_of_sub_messages() != 0:
            for message in folder.sub_messages:
                if message.subject is not None:
                    datastr = archive.format_message(message).encode("utf-8").decode("utf-8")
                    msg = email.message_from_string(datastr)
                    emailList.append(geteml.retreive_email(msg).copy())
    return emailList



if __name__ == '__main__':
    retrieve_email("../Test pst/backup.pst")