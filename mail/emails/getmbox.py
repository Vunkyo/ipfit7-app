import mailbox
import html2text
from bs4 import BeautifulSoup


dictList = []
msgDict = {}

def extractattachements(message):
    if message.get_content_maintype() == 'multipart':
        i = 0
        for part in message.walk():
            if part.get_content_maintype() == 'multipart': continue
            if part.get('Content-Disposition') is None: continue
            filename = part.get_filename()
            if filename is not None:
                msgDict['file' + str(i)] = [filename, part.get_payload(decode=True)]
                i = i + 1

def retreive_email(mbox_file):
    # print("Processing " + mbox_file)
    mbox = mailbox.mbox(mbox_file)

    for message in mbox:
        extractattachements(message)

        # print("From: " + str(message['from']))
        msgDict['From'] = str(message['From'])
        # print("To: " + str(message['to']))
        msgDict['To'] = str(message['To'])
        # print("Date: " + str(message['Date']))
        msgDict['Date'] = str(message['Date'])
        # print("Subject: " + str(message['Subject']))
        msgDict['Subject'] = str(message['Subject'])
        # print("-----------------------------")
        # print("Body\n")
        body = None
        # Walk through the parts of the email to find the text body.
        if message.is_multipart():
            for part in message.get_payload():
                if part.is_multipart():
                    for subpart in part.get_payload():
                        content = ''.join(str(subpart.get_payload()))
                        html = subpart.get_content_type() == 'text/html'
                else:
                    content = ''.join(str(part.get_payload()))
                    html = part.get_content_type() == 'text/html'
        else:
            content = str(message.get_payload())
            html = message.get_content_type() == 'text/html'

        if html is True:
            soup = BeautifulSoup(content,  'lxml')
            for script in soup(["script", "style"]):
                script.extract()  # rip it out
            content = soup.get_text()
            # break into lines and remove leading and trailing space on each
            lines = (line.strip() for line in content.splitlines())
            # break multi-headlines into a line each
            chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
            # drop blank lines
            content = '\n'.join(chunk for chunk in chunks if chunk)


        # print(content)
        msgDict['Body'] = content
        dictList.append(msgDict.copy())
        # print(msgDict)
        # print(len(dictList))
        msgDict.clear()
        # print("********************************************")
    return dictList

if __name__ == '__main__':
    mbox_file = "../Test mbox/personalgoogle.mbox"
    x = retreive_email(mbox_file)


