from mail.emails import geteml
from mail.emails import getmbox
from mail.emails import getpst
import email
import datetime
import os
import threading
from Tools import PDFFormat
from Tools import ToPDF
from Tools import checkFolderExist
currentdate = datetime.datetime.now()


def loopAttachments(item, i, dirName):
    if 'file' + str(i) in item:
        with open(dirName + item['file' + str(i)][0], 'wb') as f:
            f.write(item['file' + str(i)][1])
        i+=1
        loopAttachments(item, i, dirName)


def getAttachmentsBase(outputdir, mailList):
    checkFolderExist.checkFolderExist(outputdir)
    for item in mailList:
        loopAttachments(item, 0, outputdir)


def keywords(Emails, Keywords):
    filtered = []
    Keywords = [word for word in Keywords if word != ""]
    if not Keywords:
        return Emails

    for mail in Emails:
        for word in Keywords:
            if mail['Subject'] is not None:
                if word.lower() in mail['Subject'].lower():
                    filtered.append(mail)
                elif mail['Body'] is not None:
                    if word.lower() in mail['Body'].lower():
                        filtered.append(mail)
                    elif mail['From'] is not None:
                        if word.lower() in mail['From'].lower():
                            filtered.append(mail)
                        elif mail['To'] is not None:
                            if word.lower() in mail['To'].lower():
                                filtered.append(mail)
    return filtered


def timesfilter(Emails, datestart, dateend):
    filtered = []
    datestart = datetime.datetime.strptime(datestart, '%Y-%m-%d').date()
    dateend = datetime.datetime.strptime(dateend, '%Y-%m-%d').date()

    for mail in Emails:
        if mail['Date'] is None or mail['Date'] == 'None':
            mailDate = datestart
        elif "," in mail['Date']:
            if "(" in mail['Date']:
                mail['Date'] = mail['Date'].split("(", 1)[0]
                mailDate = datetime.datetime.strptime(mail['Date'], '%a, %d %b %Y %H:%M:%S %z ').date()
            else:
                mailDate = datetime.datetime.strptime(mail['Date'], '%a, %d %b %Y %H:%M:%S %z').date()
        elif "," not in mail['Date']:
            mailDate = datetime.datetime.strptime(mail['Date'], '%d %b %Y %H:%M:%S %z').date()


        if datestart <= mailDate <= dateend:
            filtered.append(mail.copy())
    return filtered



# Start and End date must be in "YYYY-MM-DD" Format (string)
def mailanalyse(file, outputDir, keywordList = None, datestart = str(datetime.datetime(1900, 1, 1, 0, 0, 0, 0).date()), dateend = str(currentdate.date())):
    if keywordList is None:
        keywordList = ["", "", ""]
    if datestart == '--':
        datestart = str(datetime.datetime(1900, 1, 1, 0, 0, 0, 0).date())
    if dateend == '--':
        dateend = str(currentdate.date())

    emailList = []

    checkFolderExist.checkFolderExist(outputDir + "/Mail/")

    if file.endswith('.eml'):
        print("format known")
        with open(file) as f:
            email_message = email.message_from_file(f)
            emailList = [geteml.retreive_email(email_message)]
            f.close()
    elif file.endswith('.mbox'):
        print("format known")
        emailList = getmbox.retreive_email(file)
    elif file.endswith('.pst'):
        print("format known")
        emailList = getpst.retrieve_email(file)
    else:
        print("format not yet supported")

    print([datestart])
    print([dateend])
    print(keywordList)

    timeFilteredList = timesfilter(emailList, datestart, dateend)
    keywordFilteredList = keywords(timeFilteredList, keywordList)

    threadAttachments = threading.Thread(target=getAttachmentsBase, args=(outputDir + "/Mail/Attachments/", keywordFilteredList))
    threadAttachments.start()

    format = PDFFormat.mailTemplate(keywordFilteredList)

    ToPDF.ToPDF(format, outputDir + "/Mail/", 'Mail')

    print(len(format))

if __name__ == '__main__':
    mailanalyse(file="./Test pst/backup.pst", keywordList=["", "", ""], datestart="2019-3-20", outputDir="/output/2020-06-14_12-40-23")  # , dateend="2019-03-22"
